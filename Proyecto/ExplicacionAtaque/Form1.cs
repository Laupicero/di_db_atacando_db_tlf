﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace AccesoConectado
{
  public partial class Form1 : Form
  {
    private SqlConnection conexion;
    private SqlCommand command;
    private SqlDataReader reader;
    private string strConexion = null;

    public Form1()
    {
      InitializeComponent();
    }

    private void Form1_Load(object sender, EventArgs e)
    {
            try
            {
                //string cadenaDeConexion = @"Server=DESKTOP-A795OQI;Database=bd_telefonos; Integrated Security=true;";
                string cadenaDeConexion = @"Server=DESKTOP-BK0GI3H;Database=bd_telefonos; uid=sa; pwd=2DAM2dam#";
                this.conexion = new SqlConnection(cadenaDeConexion);
                this.conexion.Open();
                MessageBox.Show("Conexion exitosa a SQL Server");
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Problema al tratar de conectar a BD. Detalles: \n" + ex.Message.ToString());
            }
        }

        // A la hora de insertar un número de TLF en el textBox
    private void BtMostrarDatos_Click(object sender, EventArgs e)
    {
            String datosTLF = "";

            if (ctSql.TextLength > 0)
            {
                //ComboBox-sexo
                this.command = new SqlCommand("Select * from telefonos WHERE telefono = '" + ctSql .Text+ "'", this.conexion);
                this.reader = this.command.ExecuteReader();

                while (this.reader.Read())
                {
                    lsTfnos.Items.Add(this.reader["telefono"].ToString());

                    datosTLF = "Nombre: " + this.reader["nombre"].ToString()
                        +"\nDIRECCIÓN: " + this.reader["direccion"].ToString()
                        + "\nTELÉFONO: " + this.reader["telefono"].ToString()
                        + "\nOBSERVACIONES: " + this.reader["observaciones"].ToString();
                }

                MessageBox.Show(datosTLF, "DATOS USUARIO", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.reader.Close();
            }
            else
                MessageBox.Show("Inserte un número de teléfono", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
    }


        //Nos muestra los datos del usuario del TLF al señalar un número
        private void lsTfnos_SelectedIndexChanged(object sender, EventArgs e)
        {
            String datosTLF = "";

            if (lsTfnos.SelectedItem != null)
            {
                //ComboBox-sexo
                this.command = new SqlCommand("Select * from telefonos WHERE telefono = '" + lsTfnos.SelectedItem.ToString() + "'", this.conexion);
                this.reader = this.command.ExecuteReader();

                while (this.reader.Read())
                {
                    lsTfnos.Items.Add(this.reader["telefono"].ToString());

                    datosTLF = "Nombre: " + this.reader["nombre"].ToString()
                        + "\nDIRECCIÓN: " + this.reader["direccion"].ToString()
                        + "\nTELÉFONO: " + this.reader["telefono"].ToString()
                        + "\nOBSERVACIONES: " + this.reader["observaciones"].ToString();
                }

                MessageBox.Show(datosTLF, "DATOS USUARIO", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.reader.Close();
            }
            else
                MessageBox.Show("Señale un número del listado", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
